<#
    .SYNOPSIS
    Short function to check if an email account has been breached utilising version 2 of Troy Hunt's API service at https://haveibeenpwned.com
    The API allows the list of pwned accounts (email addresses and usernames) to be quickly searched via a RESTful service.

    .DESCRIPTION
    Short function to check if an email account has been breached utilising version 2 of Troy Hunt's API service at https://haveibeenpwned.com
    The API allows the list of pwned accounts (email addresses and usernames) to be quickly searched via a RESTful service.

    This function queries https://haveibeenpwned.com/API/v2 API service created by Troy Hunt (@troyhunt - https://www.troyhunt.com)
    and provides the ability to produce reports on whether the Website account (email address / username) has been breached.
    Check out the outputs to see a list of fields that can be reported on.

    API Access is free but Rate Controlled. Requests to the breaches and pastes APIs are limited to one per every 1500 milliseconds each from
    any given IP address (an address may request both APIs within this period).

    (To be added - Email address validation and provide errors handling)

    .EXAMPLE
    Get-HIBPAccount -HIBPAddress email@domain.com
    Checks HIBPAddress against API and returns all Website accounts that have been pwned via the supplied email address.

    .INPUTS
    None

    .OUTPUTS
    IsRetired    : False
    IsFabricated : False
    BreachDate   : 2016-08-08
    EmailAddress : {email@domain.com}
    ModifiedDate : 2016-12-28T07:03:17Z
    PwnCount     : 4946850
    IsSpamList   : False
    Domain       : parapa.mail.ru
    IsActive     : True
    AddedDate    : 2016-12-28T07:03:17Z
    Description  : In August 2016, <a
                href="http://www.zdnet.com/article/over-25-million-accounts-stolen-after-mail-ru-forums-raided-by-hackers/" target="_blank"
                rel="noopener">the Russian gaming site known as Пара Па (or parapa.mail.ru) was hacked</a> along with a number of other
                forums on the Russian mail provider, mail.ru. The vBulletin forum contained 4.9 million accounts including usernames, email
                addresses and passwords stored as salted MD5 hashes.
    IsSensitive  : False
    Title        : Пара Па
    LogoType     : png
    IsVerified   : True
    Name         : Parapa
    DataClasses  : {Email addresses, Passwords, Usernames}

    .NOTES
    Author:     Luke Leigh
    Website:    https://blog.lukeleigh.com/
    LinkedIn:   https://www.linkedin.com/in/lukeleigh/
    GitHub:     https://github.com/BanterBoy/
    GitHubGist: https://gist.github.com/BanterBoy

    .LINK
    https://github.com/BanterBoy/PowerRepo/wiki

#>

[cmdletbinding(DefaultParameterSetName = 'default')]
param([Parameter(Mandatory = $True,
        HelpMessage = "Please enter an Email Address.",
        ValueFromPipeline = $false,
        ValueFromPipelineByPropertyName = $True)]
    [Alias('em', 'Email')]
    [string[]]$EmailAddress
)

BEGIN {}

PROCESS {
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $HIBPSite = "https://haveibeenpwned.com/api/v2/breachedaccount/"
    $HIBPResults = Invoke-RestMethod -Uri ($HIBPSite + $EmailAddress)

    foreach ($HIBPResult in $HIBPResults) {
        $HIBP = $HIBPResult | Select-Object -Property *

        try {
            $properties = @{
                Title        = $HIBP.Title
                Name         = $HIBP.Name
                Domain       = $HIBP.Domain
                BreachDate   = $HIBP.BreachDate
                AddedDate    = $HIBP.AddedDate
                ModifiedDate = $HIBP.ModifiedDate
                PwnCount     = $HIBP.PwnCount
                Description  = $HIBP.Description
                DataClasses  = $HIBP.DataClasses
                IsVerified   = $HIBP.IsVerified
                IsFabricated = $HIBP.IsFabricated
                IsSensitive  = $HIBP.IsSensitive
                IsActive     = $HIBP.IsActive
                IsRetired    = $HIBP.IsRetired
                IsSpamList   = $HIBP.IsSpamList
                LogoType     = $HIBP.LogoType
                EmailAddress = $EmailAddress
            }
        }
        catch {
            $properties = @{
                Title        = $HIBP.Title
                Name         = $HIBP.Name
                Domain       = $HIBP.Domain
                BreachDate   = $HIBP.BreachDate
                AddedDate    = $HIBP.AddedDate
                ModifiedDate = $HIBP.ModifiedDate
                PwnCount     = $HIBP.PwnCount
                Description  = $HIBP.Description
                DataClasses  = $HIBP.DataClasses
                IsVerified   = $HIBP.IsVerified
                IsFabricated = $HIBP.IsFabricated
                IsSensitive  = $HIBP.IsSensitive
                IsActive     = $HIBP.IsActive
                IsRetired    = $HIBP.IsRetired
                IsSpamList   = $HIBP.IsSpamList
                LogoType     = $HIBP.LogoType
                EmailAddress = $EmailAddress
            }
        }
        Finally {
            $obj = New-Object -TypeName PSObject -Property $properties
            Write-Output $obj
        }
    }
}

END {}
